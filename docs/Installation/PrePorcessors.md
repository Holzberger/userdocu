Installing Pre-Processors for openCFS
=====================================

openCFS can read various mesh formats. A few selected software choices are given in the following.

## Gmsh

[Gmsh](https://gmsh.info/) is an open source 3D finite element mesh generator with a built-in CAD engine and post-processor.
It is available for all common operating systems: [downloaded](https://gmsh.info/#Download) or install it directly via your package manager on Linux. 

## cubit / trelis

Coreform Cubit (formerly trelis) is a powerful mesh generator including geometry modelling and import features.
There is a free version available: download and install [Coreform Cubit Learn](https://coreform.com/products/coreform-cubit/free-meshing-software/).
