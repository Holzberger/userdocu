# Installing openCFS

openCFS runs on Linux, macOS and Windows.
Below you find instructions for your operating system.

## Linux
We provide openCFS including all dependencies in a single archive.
The software should run on any recent linux system.

To install, just download the most recent archive, e.g. the recent master build: [CFS-master-Linux.tar.gz](https://opencfs.gitlab.io/cfs/CFS-master-Linux.tar.gz), and extract it to the desired location.

```shell
wget https://opencfs.gitlab.io/cfs/CFS-master-Linux.tar.gz
tar -xzvf CFS-master-Linux.tar.gz
```

This will extract to `CFS-<SHA>-Linux` where `<sha>` is the commit checksum of the installation.
The executables are found in the `bin` directory of the installation, e.g. in the directory `CFS-<SHA>-Linux/bin`.
You can add this directory to your `PATH` by running

```
export PATH=<absolute-cfs-installation-path>/bin:$PATH
```

To make this persistent add it to the configuration file of your shell, e.g. to `~/.basrc` for bash.

You can now run cfs by
```shell
cfs -h
```

## macOS
We provide openCFS including a subsection of dependencies in a single archive.
For structural optimization you shall download the snopt and scip source from the respective code authors and compile openCFS yourself.

The provided binary is built with recent macOS (Intel) and not tested on former platforms.

For further help please follow the Linux instructions

Download a snapshot from March 2021 via this link [CFS-macOS_2021_03.tar.gz](https://faubox.rrze.uni-erlangen.de/getlink/fi7ctatGvtFGcAcSKQSKp1KN/CFS-macOS_2021_03.tar.gz)


## Windows

Currently under construction.
