#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 15:24:11 2020

@author: INTERN+feberhar
"""

import matplotlib.pyplot as plt
import math

file1 = "acoustic_ue_test-acouPotentialD1-node-7730-mic1.hist"
file2 = "acoustic_ue_test-acouPotentialD1-node-32984-mic2.hist"

fobj = open(file1, "r")
cont = fobj.read()
fobj.close()
txt = cont.split("\n")
x = []
y = []
rho = 1.204
for i in range(len(txt) - 4):
    line = txt[i + 3].split("  ")
    x.append(float(line[0]))
    yvalue = float(line[1])
    yvalue = math.log10(yvalue / rho / c ** 2 / (20 * 10 ** -6)) * 20
    y.append(yvalue)

# -----------------------------------------------------------------------------------

fobj = open(file2, "r")
cont = fobj.read()
fobj.close()
txt = cont.split("\n")
x1 = []
y1 = []

for i in range(len(txt) - 4):
    line = txt[i + 3].split("  ")
    x1.append(float(line[0]))
    yvalue = float(line[1])
    yvalue = math.log10(yvalue / rho / (20 * 10 ** -6)) * 20
    y1.append(yvalue)

# -----------------------------------------------------------------------------------

plt.plot(x, y)
plt.grid()
plt.title("mic1")
# plt.ylabel('amplitude [$m^2/s^2$]')
plt.ylabel("SPL [dB]")
plt.xlabel("frequency [$Hz$]")
# plt.legend(('mic1','mic2'))
plt.tight_layout()
if 1:
    picname = "mic1.png"
    plt.savefig(picname, dpi=100)

plt.show()

plt.plot(x1, y1)
plt.grid()
plt.title("mic2")
# plt.ylabel('amplitude [$m^2/s^2$]')
plt.ylabel("SPL [dB]")
plt.xlabel("frequency [$Hz$]")
# plt.legend(('mic1','mic2'))
plt.tight_layout()
if 1:
    picname = "mic2.png"
    plt.savefig(picname, dpi=100)

plt.show()
