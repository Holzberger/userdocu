#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import rc
#rc('text', usetex=True)
plt.rc('font', family='serif')
rc('font',size=16.0)
rc('lines',linewidth=1.5)
rc('legend',fontsize='medium',numpoints=1)     #framealpha=1.0,
rc('svg',fonttype='none')
#from scipy.interpolate import spline

sources = ["2m"] # ["Monopol", "Dipol", "Quadrupol"]
sizes = [""] # [0.05, 0.025, 0.0125]

for s in sources:
    p_max = 0
    # find maximum pressure of all sizes for one source
    for i in sizes:
        p_re = np.loadtxt('micArrayResults_'+s+''+str(i)+'-17', usecols=[3], delimiter=',',dtype=float ,skiprows=1)
        p_im = np.loadtxt('micArrayResults_'+s+''+str(i)+'-17', usecols=[4], delimiter=',',dtype=float ,skiprows=1)
        p = (p_re**2+p_im**2)**0.5
        if max(p) > p_max:
            p_max = max(p)
     
    #make the plots for each size    
    
#    p_ref_re = np.loadtxt('micArrayResults_'+s+'_0.05-1', usecols=[3], delimiter=',',dtype=float ,skiprows=1)
#    p_ref_im = np.loadtxt('micArrayResults_'+s+'_0.05-1', usecols=[4], delimiter=',',dtype=float ,skiprows=1)
#    p_ref = (p_ref_re**2+p_ref_im**2)**0.5
    
    for i in sizes:
        # use recombined sensorarray results
        p_re = np.loadtxt('micArrayResults_'+s+''+str(i)+'-17', usecols=[3], delimiter=',',dtype=float ,skiprows=1)
        p_im = np.loadtxt('micArrayResults_'+s+''+str(i)+'-17', usecols=[4], delimiter=',',dtype=float ,skiprows=1)
        p = (p_re**2+p_im**2)**0.5
        # with maximum pressure of source as reference (not logarithmic)
        p_rel = p/p_max
        theta = np.arange(0, 2*np.pi, 2*np.pi/len(p))
        
        ax = plt.subplot(111, projection='polar')
        ax.plot(theta, p_rel, label = "Size "+str(i)+" m")
    
    #Plotten:
    #ax.set_ylim([,1])
    #plt.yticks(np.arange(-20,5,5))
    #ax.set_ylabel('dB', rotation='horizontal')
    #ax.yaxis.set_label_coords(1.05,0.69)
    #plt.legend(bbox_to_anchor=(1.2, 1), loc=2, borderaxespad=0.,framealpha=0.5)
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=10.0)
    plt.savefig(s+"_finer.pdf", bbox_inches='tight', transparent=True)
    plt.show()
    plt.close()
    
    
    
